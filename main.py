# Funções do programa
from funcs import *

# Bibliotecas necessárias para processamento e criação de log
import cx_Oracle
import time

if __name__ == '__main__':
    # Inicio do contador de tempo de execução
    inicio = time.time()

    # Geração de log
    log_name = f'LOG_PROCESSAMENTO_AUTOMATICO_{time.strftime("%Y%m%d_%Hh%Mm")}.csv' # Nome
    arquivo_log = open(log_name, 'w') # Abre o arquivo como escrita
    arquivo_log.write('HORA;STATUS;CLIENTE;ARQUIVO\n') # Cabeçalho

    with cx_Oracle.connect(user='USERTI',password='copabr4s1l',dsn='prd1.vidalink.pbm/prd') as conn: # Conexão com o banco
        arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};CONECTADO\n')
        arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};PROCURANDO ARQUIVOS\n')

        # Função que percorre o Y: em busca de arquivos .txt e/ou .csv
        lista = lista_arquivos()
        clear_screen()

        # Se a lista for maior que 0 ou seja, se houver arquivos nas pastas, a condição é == True
        if len(lista) > 0: 
            arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};PROCESSANDO ARQUIVOS\n\n')

            # Para cada caminho da lista 
            for caminho in lista:
                print(f'[ {caminho} ]')
                # Separa o nome do arquivo do nome da pasta
                empresa = caminho[:caminho.find('|')]
                arquivo = caminho[caminho.find('|')+1:]

                # Procura a proc que processa o arquivo da empresa. Se a proc estiver registrada no banco, o arquivo é processado
                proc = procura_proc(conn, empresa)
                
                if proc != None:
                    try:    
                        if proc[0].endswith('PADRAO') or proc[0].endswith('LAY_PLN_Y'):
                            # Processa com a PLN_Y
                            processa_pln_y(conn, arquivo, empresa)
                            # Move o arquivo se gerou log
                            move_arquivo(empresa, arquivo)
                            arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};PROCESSADO;{empresa.upper()};{arquivo}\n')
                        else:
                            # Processa com a proc do cliente
                            processa_outras(conn, arquivo, proc)
                            # Move o arquivo se gerou log
                            move_arquivo(empresa, arquivo)
                            arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};PROCESSADO;{empresa.upper()};{arquivo}\n')
                    except Exception as error:
                        # Se der erro de atributo, registra no log
                        print(f'Error message.: {error.message} | Error args.: {error.args}')
                        arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};ERRO {error};{empresa.upper()};{arquivo}\n')
        else:
            arquivo_log.write(f'{time.strftime("%Hh%Mm%Ss")};NAO HA ARQUIVOS PARA PROCESSAR\n')

    
    arquivo_log.write(f'\n{time.strftime("%Hh%Mm%S")};FIM\n')
    arquivo_log.close() # Fecha o arquivo de log

    # Move o arquivo de log para a pasta de logs no F
    shutil.move(log_name, f'F:\Vidas\Carga\Log_de_Execucao\\{log_name}')
    
    # Fim do contador de execucao
    fim = time.time()
    duracao = fim - inicio
    print(f'\nTempo de processamento.: {round(duracao)}s')