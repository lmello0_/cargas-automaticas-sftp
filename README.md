# Processamento automático de cargas (SFTP)

O programa escaneia o SFTP e verifica se há cargas para processamento, se houver, o arquivo é processado.

Obs.: O programa consegue lidar com as exceções e para esclarecimento de dúvidas do processo, é gerado logs.

## Requerimentos

Programas:
- Python 3

Bibliotecas:
- cx_Oracle 
- shutil 
- os 
