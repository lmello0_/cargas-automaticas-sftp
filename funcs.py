import os
import shutil
import cx_Oracle
import time

def lista_arquivos():
    lista = []
    pastas = os.listdir('Y:')
    for pasta in pastas:
        if pasta.find('.txt') == -1:
            print(pasta)
            subpastas = os.listdir(f'Y:\\{pasta}')
            for subpasta in subpastas:
                if subpasta.upper() == 'USR':
                    for arquivos in os.listdir(f'Y:\\{pasta}\\{subpasta}'):
                        if arquivos.upper().endswith('.CSV') or arquivos.upper().endswith('.TXT'):
                            print(f'[ {pasta} | {arquivos} ]')
                            lista.append(f'{pasta}|{arquivos}')
    return lista

def procura_proc(conn, empresa):
        # Criando cursor de execução
        cursor = conn.cursor()

        query = cursor.execute(f"""SELECT PROCEDURE FROM DINO.PROCS_ATIVAS
                                    WHERE CUSTOMER_NAME = '{empresa.upper()}'""")

        for row in query:
            return row

def move_arquivo(pasta, file):
    controle = checa_log(pasta)

    if controle == True:
        shutil.move(f'Y:\\{pasta}\\Usr\\{file}', f'Y:\\{pasta}\\Usr\\Bkp\\{file}')
        print('Arquivo movido')
    else:
        print('Arquivo não movido')

def processa_outras(conn, arquivo, proc):
    print(f'Arquivo.: {arquivo} | Proc.: {proc[0]}')
    proc = proc[0]

    try:
        cursor = conn.cursor()
        cursor.callproc(proc, [f'{arquivo}'])
    except cx_Oracle.Error as error:
        error_obj = error.args
        print(f'Error code.: {error.code} | Error message.: {error_obj.message}')
        pass

def processa_pln_y(conn, arquivo, pasta):
    print(f'Arquivo.: {arquivo} | Proc.: USERTI.SP_CARREGA_DADOS_LAY_PLN_Y')

    try:
        cursor = conn.cursor()
        cursor.callproc('USERTI.SP_CARREGA_DADOS_LAY_PLN_Y', [f'{arquivo}', f'{pasta}'])
    except cx_Oracle.Error as error:
        error_obj = error.args
        print(f'Error code.: {error.code} | Error message.: {error_obj.message}')
        pass

def checa_log(pasta):
    data_atual = time.strftime('%d%m%y')

    for arquivo in os.listdir(f'Y:\\{pasta}\\Log'):
        # print(arquivo)
        if arquivo.find(data_atual) != -1:
            return True

def clear_screen():
    if os.name == 'POSIX':
        os.system('clear')
    elif os.name == 'nt':
        os.system('cls')